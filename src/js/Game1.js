import Phaser, { Data, Game, Tilemaps } from "phaser";
import config from "visual-config-exposer";

var fruits = [];

var fruitcollection = [config.settings.fruits][0];
var fruitcollect = Object.entries(fruitcollection);

for (var i = 0; i < fruitcollect.length; i++) {
  fruits.push(Object.entries(fruitcollection)[i][1].fruitname);
}

var fruit,
  input,
  bomb,
  pointerover,
  lives,
  livecount = config.settings.lives,
  overX,
  overY,
  downX,
  downY,
  upX,
  upY,
  splatter,
  missed,
  graphics,
  over,
  start,
  boom,
  explode,
  particles,
  tipLabel,
  fruitcut,
  fruitcut2,
  scoreLabel,
  pointer3,
  chromeLabel,
  Highscore = 0,
  timer,
  pointerup,
  fruitpoint,
  fontSize,
  fruitSize,
  score2,
  explosion,
  timerCount = config.settings.timer,
  contactPoint,
  randomh,
  objects,
  pointer,
  pointer2,
  splashs,
  score = 0,
  line,
  play,
  circle1,
  points = [];
var score3 = 0;
var random;
var randomNum = Math.floor(Math.random() * (fruits.length - 1) + 1);
var fireRate = 1400;
var nextFire = 0;
var n = 2;
localStorage.setItem("score", 0);
localStorage.setItem("Highscore", 0);
var store = parseInt(localStorage.getItem("Highscore"));
class Game1 extends Phaser.Scene {
  constructor() {
    super("playthis");
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  preload() {
    /* for(var i=0; i<fruits.length;i++) {
      var name = `fruit${i}`
      this.load.image(`fruit${i}`, config.settings.fruits.fruit[i].fruitimage);
    }*/
    this.load.image("fruit0", config.settings.fruits.fruit1.fruitimage);
    this.load.image("fruit1", config.settings.fruits.fruit2.fruitimage);
    this.load.image("fruit2", config.settings.fruits.fruit3.fruitimage);
    this.load.image("fruit3", config.settings.fruits.fruit4.fruitimage);
    this.load.image("fruit4", config.settings.fruits.fruit5.fruitimage);
    this.load.image("blade", "./public/assets/images/cut.png");
    this.load.image("explosion", "./public/assets/images/explosion.png");
    this.load.image("boom", "./public/assets/images/boom.png");
    this.load.image("splashs", "./public/assets/images/splash.png");
    this.load.image("apple-1", "./public/assets/images/fruits/apple-1.png");
    this.load.image("apple-2", "./public/assets/images/fruits/apple-2.png");
    this.load.image(
      "strawberry-1",
      "./public/assets/images/fruits/strawberry-1.png"
    );
    this.load.image(
      "strawberry-2",
      "./public/assets/images/fruits/strawberry-2.png"
    );
    this.load.image(
      "watermelon-1",
      "./public/assets/images/fruits/watermelon-1.png"
    );
    this.load.image(
      "watermelon-2",
      "./public/assets/images/fruits/watermelon-2.png"
    );
    this.load.image("peach-1", "./public/assets/images/fruits/peach-1.png");
    this.load.image("peach-2", "./public/assets/images/fruits/peach-2.png");
    this.load.image("banana-1", "./public/assets/images/fruits/banana-1.png");
    this.load.image("banana-2", "./public/assets/images/fruits/banana-2.png");
  }

  create(event) {
    console.log(config.settings.fruits);
    //timer = 10;
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;
    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        (this.GAME_WIDTH = 740), //600
        (this.GAME_HEIGHT = 900), //1000
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on("resize", this.resize, this);

    /*this.physics.world.setBounds(0, 0, this.GAME_WIDTH, this.GAME_HEIGHT);*/

    let bg = this.add.sprite(0, 0, "background");
    bg.setOrigin(0, 0).setDisplaySize(740, 1000);
    bg.depth = 0;
    splatter = this.sound.add("splash");
    missed = this.sound.add("missed");
    over = this.sound.add("over");
    start = this.sound.add("start");
    start.play();
    explode = this.sound.add("explode");
    scoreLabel = this.add.text(150, 50, `Score: ${score}`, {
      fontSize: "30px",
    });

    splashs = this.add.image(0, 0, "splashs").setDisplaySize(100, 100);
    splashs.visible = false;

    /*  Highscore = this.add.text(50, 10, `Highscore: ${Highscore}`, {
      fontSize: "30px",
    });*/

    //pointer3 = this.add.line(200, 200, 0, 0, -50, 100, 0xffffff);

    // pointer2.visible = false;

    this.graphics2 = this.add.graphics();
    this.graphics2.lineStyle(4, 0x808080, 0.3);

    this.graphics2.strokeCircle(50, 50, 10, 10);
    this.graphics2.fillStyle(0x00ff00, 0.5);
    this.graphics2.strokeCircle(event.worldX, 200, 10, 10);
    this.graphics2.fillCircle(event.worldX, 200, 10);
    this.graphics2.strokePath();

    pointer2 = this.add.image(200, 200, "blade").setDisplaySize(50, 100);
    pointer2.visible = false;

    timer = this.add.text(400, 50, `Timer: ${timerCount}`, {
      fontSize: "30px",
    });
    lives = this.add.text(400, 100, `Lives: ${livecount}`, {
      fontSize: "30px",
    });
    if (window.innerWidth < 300) {
      scoreLabel.x = 300;
      timer.x = 300;
      timer.y = 100;
      lives.x = 300;
      lives.y = 150;
    }
    setInterval(() => {
      if (timerCount == 0) {
        this.scene.start("endGame");
        over.play();
        if (livecount == 0) {
          this.scene.start("endGame");
          over.play();
        }
      }

      timerCount--;
    }, 1000);

    setInterval(() => {
      for (var n = 1; n < 2; n++) {
        var num = Math.floor(Math.random() * 15 + 1);
        bomb = this.physics.add
          .image(50 * num, -50 * num, "bomb")
          .setDisplaySize(80, 80);
        boom = this.physics.add
          .image(50 * num, -50 * num, "boom")
          .setDisplaySize(80, 80);
        boom.setVisible(false);
      }
    }, 2000);

    setInterval(() => {
      for (var n = 1; n < 2; n++) {
        var num = Math.floor(Math.random() * (fruits.length - 1) + 0);

        fruit = this.physics.add
          .sprite(50 * (num + 3), -1 * num, `fruit${num}`)
          .setDisplaySize(80, 80);

        var name = fruits[num];

        fruitcut = this.physics.add.sprite(
          50 * (num + 2),
          -1 * num,
          `${name}-1`
        );
        fruitcut.visible = false;
        fruitcut2 = this.physics.add.sprite(
          50 * (num + 3),
          3 * num,
          `${name}-2`
        );
        fruitcut2.visible = false;

        particles = this.physics.add
          .sprite(50 * (num + 2), -1 * num, "explosion")
          .setDisplaySize(100, 100)
          .setVisible(false);
      }
    }, 2000);
  }

  update(event) {
    score2 = localStorage.getItem("score");
    //console.log(this.input.x)
    timer.text = `Timer : ${timerCount}`;

    pointer = this.input.on("pointerdown", function (pointer) {
      downX = pointer.x;
      downY = pointer.y;
    });

    pointerover = this.input.on("pointermove", function (pointerover) {
      overX = pointerover.x + 130;
      overY = pointerover.y;
      //console.log(fruit.x)
      //console.log(overX)

      if (
        /*fruit.x + 100 >= pointer.worldX &&
        fruit.x - 100 <= pointer.worldX &&
        fruit.y + 100 >= pointer.worldY &&
        fruit.y - 100 <= pointer.worldY &&*/
        //(upX < downX - 50 || upX > downX - 50) &&
        overX <= fruit.x + 50 &&
        overX >= fruit.x - 50 &&
        overY <= fruit.y + 50 &&
        overY >= fruit.y - 50 &&
        window.innerWidth < 700
      ) {
        //  console.log(`kat gaya ${overX}`)
        splashs.x = pointer.worldX;
        splashs.y = pointer.worldY;
        splashs.visible = true;

        fruitcut.visible = true;
        fruitcut2.visible = true;
        score3 = parseInt(score2) + config.settings.Points;
        localStorage.setItem("score", score3);
        scoreLabel.text = `Score: ${score3}`;
        fruit.destroy();
        splatter.play();
        // particles.visible = true;
      } else {
        if (window.innerWidth < 700) {
          missed.play();
          pointer2.x = downX + 200;
          pointer2.y = downY + 200;
          pointer2.visible = true;
          setTimeout(() => {
            pointer2.visible = false;
          }, 100);
        }
        this.input.enabled = false;
        pointer2.visible = true;
      }
    });

    this.graphics = this.add.graphics();
    this.graphics.lineStyle(10, 0x808080, 0.1);
    setTimeout(() => {
      this.graphics.destroy();
      this.graphics.visible = false;
    }, 5);
    // this.graphics.fillStyle(0xff0ff, .5)
    /* this.graphics.strokeCircle(50,50,10,10) 
  this.graphics.fillStyle(0xff0ff, .5)
  this.graphics.strokeCircle(event.worldX,200,10,10)
  this.graphics.fillCircle(event.worldX,200,10)*/
    this.graphics.lineTo(downX - 400, downY + 100);
    this.graphics.lineTo(upX - 400, upY + 100);

    /*this.graphics.moveTo(event.worldX,event.worldY);
  this.graphics.lineTo(event.worldX+50,event.worldY+50)*/
    this.graphics.strokePath();

    this.graphics2.x = this.input.x - 400;
    this.graphics2.y = this.input.y;

    /*this.graphics1.x = downX-400
    this.graphics1.y = downY-400
   /*this.graphics.moveTo(this.input.x-400, this.input.x-400)
   this.graphics.lineTo(upX-500, upY-500)*/

    pointerup = this.input.on("pointerup", function (pointer) {
      upX = pointerup.x;
      upY = pointerup.y;

      if (
        fruit.x + 100 >= pointer.worldX &&
        fruit.x - 100 <= pointer.worldX &&
        fruit.y + 100 >= pointer.worldY &&
        fruit.y - 100 <= pointer.worldY &&
        (upX < downX - 50 || upX > downX - 50) &&
        window.innerWidth > 700
        /* (overX<=fruit.x+100 && overX>=fruit.x-100) ||
       (overY<=fruit.y+100 && overY>=fruit.y-100) */
      ) {
        splashs.x = pointer.worldX;
        splashs.y = pointer.worldY;
        splashs.visible = true;

        fruitcut.visible = true;
        fruitcut2.visible = true;
        score3 = parseInt(score2) + config.settings.Points;
        localStorage.setItem("score", score3);
        scoreLabel.text = `Score: ${score3}`;
        fruit.destroy();
        splatter.play();
        // particles.visible = true;
      }
      if (
        (bomb.x + 100 >= pointer.worldX &&
          bomb.x - 100 <= pointer.worldX &&
          bomb.y + 100 >= pointer.worldY &&
          bomb.y - 100 <= pointer.worldY &&
          (upX < downX - 50 || upX > downX - 50)) ||
        (overX <= bomb.x + 50 &&
          overX >= bomb.x - 50 &&
          overY <= bomb.y + 50 &&
          overY >= bomb.y - 50 &&
          window.innerWidth < 700)
      ) {
        livecount - 1;
        bomb.destroy();
        explode.play();
        boom.visible = true;
        score3 = parseInt(score2) - config.settings.Points;
        localStorage.setItem("score", score3);
        scoreLabel.text = `Score: ${score3}`;
        lives.text = `Lives: ${livecount - 1}`;
      } else {
        missed.play();
        this.input.enabled = false;
      }
      if (score3 > store) {
        localStorage.setItem("Highscore", score3);
      }

      setTimeout(() => {
        splashs.visible = false;
      }, 800);
    });

    random = Math.floor(Math.random() * fruits.length + 1);
  }

  storeScore() {
    localStorage.setItem("score", 0);
  }
  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }
}

export default Game1;
